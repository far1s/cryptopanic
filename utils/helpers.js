module.exports = {
    getAllCurrencies: function(allPosts) {
        const currencies = [];
        for (let i = 0; i < allPosts.length; i++) {
            const element = allPosts[i];
            const curr = element && element.currencies
            ? element.currencies
            : [];
            curr.forEach(currency => {
                currencies.push(currency.code);
            });
        }
        return currencies;
    },
    getOccurrence: function(allCurrencies) {
        if (!allCurrencies || allCurrencies.length === 0) {
            return [];
        }
        const occurrence = [];

        allCurrencies.sort();

        let previous;
        for (let i = 0; i < allCurrencies.length; i++) {
            const current = allCurrencies[i];
            if (current !== previous) {
                occurrence.push({
                    coin: current,
                    occurrence: 1,
                });
            } else {
                occurrence[occurrence.length - 1].occurrence++;
            }
            previous = current;
        }
        return occurrence;
    }
}
