const express = require('express');
const axios = require('axios');
const app = express();
const {
    getAllCurrencies,
    getOccurrence,
} = require('./utils/helpers.js');

async function getPostsByPage(page) {
    return await axios.get(
        `https://cryptopanic.com/api/posts/?auth_token=4737cb1a9e4531d7c30e0898c951dd68ce26db7d&page=${page}`
    );
}

app.get('/', async (req, res) => {
    const amountPosts = 20;

    let allPosts = [];
    for (let index = 0; index < amountPosts; index++) {
        console.log('index: ', index);
        const posts = await getPostsByPage(index + 1);
        allPosts.push(...posts.data.results);
    }

    const allCurrencies = getAllCurrencies(allPosts);
    const occurrences = getOccurrence(allCurrencies);
    occurrences.sort((a, b) => b.occurrence - a.occurrence);
    res.send(occurrences);
});

app.listen(
    3000,
    () => console.log('Example app listening on port 3000!')
);
