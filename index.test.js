const {
    getAllCurrencies,
    getOccurrence,
} = require('./utils/getAllCurrencies.js');

const fs = require('fs');

let data = {};
beforeEach(() => {
    data = JSON.parse(fs.readFileSync('mock.json', 'utf8'));
});

describe('testing', () => {
    it('getAllCurrencies', () => {
        const allCurrencies = getAllCurrencies(data);
        expect(data).toBeTruthy();
        const expected =[
            'POLY', 'XRP', 'XLM', 'ICX', 'EOS', 'BTC', 'BTC',
            'ICX', 'BTC', 'EOS', 'ADA', 'EOS', 'BTC', 'BTC'
        ];
        expect(allCurrencies).toEqual(expected);
    });

    it('getOccurrence', () => {
        const allCurrencies = getAllCurrencies(data);
        const occurrence = getOccurrence(allCurrencies);
        const expected = [{
            "coin": "ADA",
            "occurrence": 1
        }, {
            "coin": "BTC",
            "occurrence": 5
        }, {
            "coin": "EOS",
            "occurrence": 3
        }, {
            "coin": "ICX",
            "occurrence": 2
        }, {
            "coin": "POLY",
            "occurrence": 1
        }, {
            "coin": "XLM",
            "occurrence": 1
        }, {
            "coin": "XRP",
            "occurrence": 1
        }];
        expect(occurrence).toBeTruthy();
        expect(occurrence).toEqual(expected);
    });
});
